/*
You can declare functions by using:
	Function keyword
	Function name
	open/close parenthesis
	open/close curly braces*/

function sayHello(){
	console.log('Hello there!')
}
// you can invoke a function by calling its function name and including the parenthesis

sayHello()

// you can assign a function to a variable. The function name would not be required

let sayGoodbye = function(){
	console.log('Goodbye!')
}
sayGoodbye()

// you can also re-assign a function as a new value of a variable

sayGoodbye = function(){
	console.log('Au Revoir!')
}

sayGoodbye()

// Declaring a constant variable with a function as a value will not allow that function to be changed or re-assigned
const sayHelloInJapanese = function(){
	console.log('Ohayo!')
}

sayHelloInJapanese()


// Global Scope - You can use a variable inside a function if the variable is declared outside of it
let action = 'Run'
function doSomethingRandom(){
	
	console.log(action)
}

doSomethingRandom()

// Local/Function scope - You cannot use a variable outside of a function if it is within the function scope/curly braces

function doSomethingRandom(){
	let action = 'Run'
	
}
console.log(action)

// Nested functions - can be done as long as you invoke the nested function within the scope of parent function
function viewProduct(){
	
	console.log('Viewing a product')
	function addToCart(){
		console.log('Added product to cart')


	}

	addToCart()
}

viewProduct()

// Alert function is a built-in javascript function where we can show alerts to the user
function singASong(){
	alert('La la la')
}

singASong()

// Any statement like 'console.log' will only run after the alert has been closed
console.log('Clap clap clap')


// Prompt is a built-in javascript function that we can use to 
function enterUserName(){
	let userName = prompt('Enter your username')
	console.log(userName)
}

enterUserName()

// You can use a prompt outside of a function. Make sure to output the prompt value by assigning to a vairable and using console.log
let userAge = prompt('Enter your age')
console.log(userAge)